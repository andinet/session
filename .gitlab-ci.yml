include:
    # Metadata shared my many jobs
    - local: .gitlab/rules.yml
    - local: .gitlab/artifacts.yml

    # OS builds.
    - local: .gitlab/os-linux.yml
    - local: .gitlab/os-macos.yml
    - local: .gitlab/os-windows.yml

stages:
    - build
    - test
    - analyze

################################################################################
# Job declarations
#
# Each job must pull in each of the following keys:
#
#   - a "base image"
#   - a build script
#   - tags for the jobs
#   - rules for when to run the job
#
# Additionally, jobs may also contain:
#
#   - artifacts
#   - dependency/needs jobs for required jobs
################################################################################

# Linux

## Build and test

build:fedora32:
    extends:
        - .fedora32_plain
        - .cmake_build_linux
        - .cmake_build_artifacts
        - .linux_builder_tags
        - .run_automatically

test:fedora32:
    extends:
        - .fedora32_plain
        - .cmake_test_linux
        - .linux_test_tags
        - .run_automatically
    dependencies:
        - build:fedora32
    needs:
        - build:fedora32

## Lint builds

build:fedora32-asan:
    extends:
        - .fedora32_asan
        - .cmake_build_linux
        - .cmake_build_artifacts
        - .linux_builder_tags
        - .run_automatically

test:fedora32-asan:
    extends:
        - .fedora32_asan
        - .cmake_memcheck_linux
        - .linux_test_priv_tags
        - .run_automatically
    dependencies:
        - build:fedora32-asan
    needs:
        - build:fedora32-asan

build:fedora32-ubsan:
    extends:
        - .fedora32_ubsan
        - .cmake_build_linux
        - .cmake_build_artifacts
        - .linux_builder_tags
        - .run_automatically

test:fedora32-ubsan:
    extends:
        - .fedora32_ubsan
        - .cmake_memcheck_linux
        - .linux_test_tags
        - .run_automatically
    dependencies:
        - build:fedora32-ubsan
    needs:
        - build:fedora32-ubsan

build:fedora32-tidy:
    extends:
        - .fedora32_tidy
        - .cmake_build_linux
        - .linux_builder_tags
        - .run_automatically
    # ITK causes compilation failures with CMake's clang-tidy support. Allow it
    # to fail, but still show the warnings.
    # https://github.com/InsightSoftwareConsortium/ITK/issues/595
    allow_failure: true

build:fedora32-coverage:
    extends:
        - .fedora32_coverage
        - .cmake_build_linux
        - .cmake_build_artifacts
        - .linux_builder_tags
        - .run_automatically

test:fedora32-coverage:
    extends:
        - .fedora32_coverage
        - .cmake_test_linux
        - .cmake_coverage_artifacts
        - .linux_test_tags
        - .run_automatically
    dependencies:
        - build:fedora32-coverage
    needs:
        - build:fedora32-coverage

analyze:fedora32-coverage:
    extends:
        - .fedora32_coverage
        - .cmake_coverage_linux
        - .linux_builder_tags
        - .run_automatically
    dependencies:
        - test:fedora32-coverage
    needs:
        - test:fedora32-coverage

# macOS

## Build and test

build:macos:
    extends:
        - .macos_plain
        - .cmake_build_macos
        - .cmake_build_artifacts
        - .macos_builder_tags
        - .run_automatically

test:macos:
    extends:
        - .macos_plain
        - .cmake_test_macos
        - .macos_builder_tags
        - .run_automatically
    dependencies:
        - build:macos
    needs:
        - build:macos

# Windows

## Build and test

build:windows-vs2019-ninja:
    extends:
        - .windows_vs2019_ninja
        - .cmake_build_windows
        - .cmake_build_artifacts
        - .windows_builder_tags
        - .run_automatically

test:windows-vs2019-ninja:
    extends:
        - .windows_vs2019_ninja
        - .cmake_test_windows
        - .windows_builder_tags
        - .run_automatically
    dependencies:
        - build:windows-vs2019-ninja
    needs:
        - build:windows-vs2019-ninja
