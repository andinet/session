# macOS has some problem that causes sccache to fail to start its server
# sometimes. Debugging tools are anemic, so hopefully this gets fixed in an
# sccache update and we can just remove this someday.
if (NOT "$ENV{CMAKE_CONFIGURATION}" MATCHES "macos")
  set(CMAKE_C_COMPILER_LAUNCHER "sccache" CACHE STRING "")
  set(CMAKE_CXX_COMPILER_LAUNCHER "sccache" CACHE STRING "")

  set(qt5_SKIP_PCH "ON" CACHE BOOL "")
endif ()
