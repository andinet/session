# This is run before smtkAEVASession is declared as a target.
# Add any VTK modules here that do not depend on smtkAEVASession.
set(module_files
  "${CMAKE_CURRENT_SOURCE_DIR}/vtk/aeva/ext/vtk.module"
  "${CMAKE_CURRENT_SOURCE_DIR}/vtk/meshing/netgen/vtk.module"
)
vtk_module_scan(
  MODULE_FILES ${module_files}
  PROVIDES_MODULES aeva_vtk_modules
  HIDE_MODULES_FROM_CACHE ON
  WANT_BY_DEFAULT ON
)
vtk_module_build(
  MODULES ${aeva_vtk_modules}
  PACKAGE AEVAVTKModules
  INSTALL_EXPORT AEVAVTKModules
  CMAKE_DESTINATION ${CMAKE_INSTALL_CMAKEDIR}
  HEADERS_DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}/${PROJECT_VERSION}"
  TEST_DIRECTORY_NAME "NONE"
)

set_property(GLOBAL
  PROPERTY
    _aeva_vtk_extension_modules "${aeva_vtk_modules}"
)

# if (SMTK_ENABLE_PYTHON_WRAPPING)
#   set(vtk_python_wrappable_modules
#     vtkAEVAWidgets)
#   set(vtk_modules_to_wrap)
#   foreach (vtk_python_wrappable_module IN LISTS vtk_python_wrappable_modules)
#     if (TARGET "${vtk_python_wrappable_module}")
#       list(APPEND vtk_modules_to_wrap
#         "${vtk_python_wrappable_module}")
#     endif ()
#   endforeach ()
#   vtk_module_wrap_python(
#     MODULES         ${vtk_modules_to_wrap}
#     INSTALL_EXPORT  smtk
#     INSTALL_HEADERS OFF
#     PYTHON_PACKAGE  "smtk.session.aeva.vtk._modules"
#     #MODULE_DESTINATION  "${PARAVIEW_PYTHON_SITE_PACKAGES_SUFFIX}"
#     #CMAKE_DESTINATION   "${paraview_cmake_destination}/vtk"
#     WRAPPED_MODULES vtk_python_wrapped_modules
#     TARGET          vtkaevapythonmodules)
#   add_subdirectory(pybind11)
# endif()
