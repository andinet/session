//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef __smtk_session_aeva_CreateAnnotationResource_h
#define __smtk_session_aeva_CreateAnnotationResource_h

#include "smtk/session/aeva/Operation.h"
#include "smtk/session/aeva/Resource.h"

namespace smtk
{
namespace session
{
namespace aeva
{

/**\brief Create an aeva attribute resource.
  *
  * This operation creates an attribute resource, populates it with a simbuilder
  * template file for anatomical annotation, and either links it to the associated
  * model resource or creates a new model resource and links to it to that.
  */
class SMTKAEVASESSION_EXPORT CreateAnnotationResource : public Operation
{

public:
  smtkTypeMacro(smtk::session::aeva::CreateAnnotationResource);
  smtkCreateMacro(CreateAnnotationResource);
  smtkSharedFromThisMacro(smtk::operation::Operation);
  smtkSuperclassMacro(Operation);

protected:
  CreateAnnotationResource();
  Result operateInternal() override;
  const char* xmlDescription() const override;

  Result m_result;
};

}
}
}

#endif
