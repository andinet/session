//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/session/aeva/operators/EditFreeformAttributes.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/ComponentItem.h"
#include "smtk/attribute/DoubleItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/StringItem.h"

#include "smtk/session/aeva/EditFreeformAttributes_xml.h"

using smtk::attribute::DoubleItem;
using smtk::attribute::IntItem;
using smtk::attribute::StringItem;
using smtk::model::Float;
using smtk::model::FloatList;
using smtk::model::Integer;
using smtk::model::IntegerList;
using smtk::model::String;
using smtk::model::StringList;

namespace smtk
{
namespace session
{
namespace aeva
{

template<typename V, typename VL, typename VI>
void EditFreeformAttributesValue(const std::string& name,
  typename VI::Ptr item,
  smtk::attribute::ReferenceItemPtr& entities)
{
  if (!item || item->numberOfValues() == 0)
  {
    // Erase the property of this type from these entities,
    // if they had the property in the first place.
    for (smtk::resource::PersistentObjectPtr entity : *entities)
    {
      entity->properties().erase<VL>(name);
    }
  }
  else
  {
    // Get the array of values from the item.
    VL values;
    values.reserve(item->numberOfValues());
    for (std::size_t i = 0; i < item->numberOfValues(); ++i)
    {
      values.push_back(item->value(i));
    }

    // Add or overwrite the property with the values.
    for (smtk::resource::PersistentObjectPtr entity : *entities)
    {
      entity->properties().get<VL>()[name] = values;
    }
  }
}

template<typename V, typename VI>
void EditFreeformAttributesValue(const std::string& name,
  typename VI::Ptr item,
  smtk::attribute::ReferenceItemPtr& entities)
{
  if (!item || item->numberOfValues() == 0)
  {
    // Erase the property of this type from these entities,
    // if they had the property in the first place.
    for (smtk::resource::PersistentObjectPtr entity : *entities)
    {
      entity->properties().erase<V>(name);
    }
  }
  else
  {
    // Get the array of values from the item.
    V value = item->value();

    // Add or overwrite the property with the values.
    for (smtk::resource::PersistentObjectPtr entity : *entities)
    {
      entity->properties().get<V>()[name] = value;
    }
  }
}

EditFreeformAttributes::Result EditFreeformAttributes::operateInternal()
{
  smtk::attribute::StringItemPtr nameItem = this->parameters()->findString("name");
  smtk::attribute::StringItemPtr stringItem = this->parameters()->findString("string value");
  smtk::attribute::DoubleItemPtr floatItem = this->parameters()->findDouble("float value");
  smtk::attribute::IntItemPtr integerItem = this->parameters()->findInt("integer value");

  auto associations = this->parameters()->associations();

  if (nameItem->value(0).empty())
  {
    return this->createResult(smtk::operation::Operation::Outcome::FAILED);
  }

  if (stringItem->numberOfValues() == 0)
  {
    EditFreeformAttributesValue<String, StringItem>(nameItem->value(0), stringItem, associations);
    EditFreeformAttributesValue<String, StringList, StringItem>(
      nameItem->value(0), stringItem, associations);
  }
  else if (stringItem->numberOfValues() == 1)
  {
    EditFreeformAttributesValue<String, StringItem>(nameItem->value(0), stringItem, associations);
  }
  else
  {
    EditFreeformAttributesValue<String, StringList, StringItem>(
      nameItem->value(0), stringItem, associations);
  }

  if (floatItem->numberOfValues() == 0)
  {
    EditFreeformAttributesValue<Float, DoubleItem>(nameItem->value(0), floatItem, associations);
    EditFreeformAttributesValue<Float, FloatList, DoubleItem>(
      nameItem->value(0), floatItem, associations);
  }
  else if (floatItem->numberOfValues() == 1)
  {
    EditFreeformAttributesValue<Float, DoubleItem>(nameItem->value(0), floatItem, associations);
  }
  else
  {
    EditFreeformAttributesValue<Float, FloatList, DoubleItem>(
      nameItem->value(0), floatItem, associations);
  }

  if (integerItem->numberOfValues() == 0)
  {
    EditFreeformAttributesValue<Integer, IntItem>(nameItem->value(0), integerItem, associations);
    EditFreeformAttributesValue<Integer, IntegerList, IntItem>(
      nameItem->value(0), integerItem, associations);
  }
  else if (integerItem->numberOfValues() == 1)
  {
    EditFreeformAttributesValue<Integer, IntItem>(nameItem->value(0), integerItem, associations);
  }
  else
  {
    EditFreeformAttributesValue<Integer, IntegerList, IntItem>(
      nameItem->value(0), integerItem, associations);
  }

  Result result = this->createResult(smtk::operation::Operation::Outcome::SUCCEEDED);

  smtk::attribute::ComponentItemPtr modifiedItem = result->findComponent("modified");
  for (smtk::resource::PersistentObjectPtr association : *associations)
  {
    if (auto component = std::dynamic_pointer_cast<smtk::resource::Component>(association))
    {
      modifiedItem->appendValue(component);
    }
  }

  return result;
}

const char* EditFreeformAttributes::xmlDescription() const
{
  return EditFreeformAttributes_xml;
}

}
}
}
