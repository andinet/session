<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the "SetProperty" Operation -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <!-- Operation -->
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="edit freeform attributes" Label="Set Property" BaseType="operation">
      <AssociationsDef Name="Entities" NumberOfRequiredValues="1" Extensible="true">
        <Accepts><Resource Name="smtk::resource::Resource"/></Accepts>
      </AssociationsDef>
      <BriefDescription>
        Set (or remove) an attribute value on a component.
      </BriefDescription>
      <DetailedDescription>
        Set (or remove) an attribute value on a component.
        The string, integer, and floating-point values are all optional.
        Any combination may be specified.
        All that are specified are set; those unspecified (i.e., those with
        zero values of the given type) are removed.

        For example, specifying both a string and an integer value for
        the "foo" attribute would set those values in the resource's
        string and integer maps while removing "foo" from the associated
        entities' floating-point map.
      </DetailedDescription>
      <ItemDefinitions>
        <String Name="name" NumberOfRequiredValues="1">
          <BriefDescription>The name of the attribute to set.</BriefDescription>
        </String>
        <Double Name="float value" NumberOfRequiredValues="0" Extensible="true">
          <BriefDescription>Floating-point value(s) of the attribute.</BriefDescription>
        </Double>
        <String Name="string value" NumberOfRequiredValues="0" Extensible="true">
          <BriefDescription>String value(s) of the attribute.</BriefDescription>
        </String>
        <Int Name="integer value" NumberOfRequiredValues="0" Extensible="true">
          <BriefDescription>Integer value(s) of the attribute.</BriefDescription>
        </Int>
      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(edit freeform attributes)" BaseType="result"/>
  </Definitions>
  <Views>
     <!--
      The customized view "Type" needs to match the plugin's VIEW_NAME:
      add_smtk_ui_view(...  VIEW_NAME EditFreeformAttributesView ...)
      -->
    <View Type="EditFreeformAttributesView" Title="Freeform Properties"
      FilterByCategory="false"  FilterByAdvanceLevel="false" UseSelectionManager="true">
      <Description>
        Enter an attribute name and type to add or remove.
        The list beneath the editor shows properties currently present on the selected components.
      </Description>
      <AttributeTypes>
        <Att Type="edit freeform attributes"/>
      </AttributeTypes>
    </View>
  </Views>
</SMTK_AttributeResource>
