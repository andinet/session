//=============================================================================
//
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//=============================================================================
#ifndef smtk_session_aeva_plugin_Registrar_h
#define smtk_session_aeva_plugin_Registrar_h

#include "smtk/view/Manager.h"

namespace smtk
{
namespace session
{
namespace aeva
{
namespace plugin
{

class Registrar
{
public:
  static void registerTo(const smtk::view::Manager::Ptr& viewManager);
  static void unregisterFrom(const smtk::view::Manager::Ptr& viewManager);
};

}
}
}
}

#endif // smtk_session_aeva_plugin_Registrar_h
